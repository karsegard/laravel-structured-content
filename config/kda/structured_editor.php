<?php

return [
    'field_types' => [
        'text' => ['label' => 'Text'],
        'editorjs' => ['label' => 'Editeur de block', 'view_namespace' => 'kda-backpack-custom-fields::fields'],
        'checkbox' => ['label' => 'Case à cocher'],
        'browse' => ['label' => 'Fichier'],
      //  'media_browse' => ['label' => 'Media','type'=>'browse'],
       // 'button' => ['label' => 'Bouton','key'=>'button'],
        'select2_from_array' => ['label'=>'Liste'],
        /*  'imagecrop'=> ['label'=> 'Image croppable','view_namespace'=>'kda-backpack-custom-fields::fields','options'=>[
            'metadata_field'=>'meta',
            'original_field'=>'original',
            'ratio'=>1,
            'crop'=>true
        ]],*/
    ],

    'table_type'=> 'scb_types',
    'template_prefix'=>'sbc'

];
