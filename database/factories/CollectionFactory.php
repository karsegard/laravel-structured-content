<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\Collection;

class CollectionFactory extends Factory
{
    protected $model = Collection::class;

    public function definition()
    {
        return [
         
        ];
    }
}
