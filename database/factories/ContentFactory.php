<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\Content;

class ContentFactory extends Factory
{
    protected $model = Content::class;

    public function definition()
    {
        return [
         
        ];
    }
}
