<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\Section;

class SectionFactory extends Factory
{
    protected $model = Section::class;

    public function definition()
    {
        return [
         
        ];
    }
}
