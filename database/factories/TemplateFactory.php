<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\Template;

class TemplateFactory extends Factory
{
    protected $model = Template::class;

    public function definition()
    {
        return [
         
        ];
    }
}
