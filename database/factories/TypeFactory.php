<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\Type;

class TypeFactory extends Factory
{
    protected $model = Type::class;

    public function definition()
    {
        return [
         
        ];
    }
}
