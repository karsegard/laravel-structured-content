<?php

namespace KDA\SBC\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use KDA\SBC\Models\ViewComposer;

class ViewComposerFactory extends Factory
{
    protected $model = ViewComposer::class;

    public function definition()
    {
        return [
         
        ];
    }
}
