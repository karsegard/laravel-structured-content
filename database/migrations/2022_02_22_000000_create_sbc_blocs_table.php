<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSbcBlocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_blocs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('type_id')->nullable()->constrained('sbc_types')->onDelete('cascade');
            $table->foreignId('layout_type_id')->nullable()->constrained('sbc_types')->onDelete('set null');
            $table->boolean('is_craftable')->default(0);
            $table->boolean('is_linkable')->default(0);
            $table->string('hint')->nullable();
            $table->string('template')->nullable();
            $table->timestamps();
        });


        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('sbc_blocs');
        Schema::enableForeignKeyConstraints();

    }
}
