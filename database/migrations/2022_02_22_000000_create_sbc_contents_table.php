<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSbcContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bloc_id')->constrained('sbc_blocs')->onDelete('cascade');
            $table->text('value')->nullable();
            $table->date('start_on')->nullable();
            $table->date('end_on')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_contents');


        Schema::enableForeignKeyConstraints();
    }
}
