<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSbcSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_sections', function (Blueprint $table) {
            $table->id();
           
       //     $table->foreignId('page_id')->constrained('static_pages')->onDelete('restrict');
            $table->nullableNumericMorphs('document');
            $table->foreignId('page_id')->constrained('sbc_pages')->onDelete('cascade');
            $table->foreignId('bloc_id')->constrained('sbc_blocs')->onDelete('cascade');
            $table->text('layout')->nullable();
            $table->nestable();

            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_sections');
        Schema::enableForeignKeyConstraints();

    }
}
