<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSbcSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_slots', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_id')->constrained('sbc_types')->onDelete('cascade');
            $table->string('name');
        });

        Schema::create('sbc_slot_types', function (Blueprint $table) {
            $table->id();
            $table->foreignId('slot_id')->constrained('sbc_slots')->onDelete('cascade');
            $table->foreignId('type_id')->constrained('sbc_types')->onDelete('cascade');
            $table->enum('rule',['accept','deny'])->default('accept');
            $table->integer('default')->default(0);
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_slots');
        Schema::dropIfExists('sbc_slot_types');
        Schema::enableForeignKeyConstraints();
    }
}
