<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSbcTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('collection_id')->nullable()->constrained('sbc_collections')->onDelete('set null');

            $table->string('description')->nullable();
            $table->text('fields')->nullable();
            $table->text('repeatable')->nullable();
            $table->boolean('is_type')->default(1);
            $table->boolean('is_layout')->default(0);
            $table->boolean('is_block')->default(0);
            $table->string('template')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_types');

        Schema::enableForeignKeyConstraints();
    }
}
