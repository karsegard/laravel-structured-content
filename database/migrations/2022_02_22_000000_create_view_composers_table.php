<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewComposersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_view_composers', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->string('class');
            $table->timestamps();
        });


        Schema::create('sbc_bloc_view_composer', function (Blueprint $table) {
            $table->foreignId('bloc_id')->constrained('sbc_blocs')->onDelete('cascade');
            $table->foreignId('composer_id')->constrained('sbc_view_composers')->onDelete('cascade');
        });

        Schema::create('sbc_bloc_slot', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bloc_id')->constrained('sbc_blocs')->onDelete('cascade');
            $table->foreignId('slot_bloc_id')->constrained('sbc_blocs')->onDelete('cascade');
            $table->foreignId('slot_id')->constrained('sbc_slots')->onDelete('cascade');
            $table->integer('sort')->nullable();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_view_composers');
        Schema::dropIfExists('sbc_bloc_view_composer');
        Schema::dropIfExists('sbc_bloc_slot');
        Schema::enableForeignKeyConstraints();
    }
}
