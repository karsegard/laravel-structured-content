<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sbc_templates', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('description')->nullable();
            $table->text('test_data')->nullable();
            $table->timestamps();
        });



        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sbc_templates');
       
        Schema::enableForeignKeyConstraints();
    }
}
