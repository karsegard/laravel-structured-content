<?php
use KDA\SBC\Facades\StructuredContent;

if (!function_exists('sc_config')) {
    function sc_config($key, $default = null)
    {
        $basekey = 'kda.sbc';
        return config($basekey . '.' . $key, $default);
    }
}



if (!function_exists('sc_absolute_path')) {
    function sc_absolute_path($path)
    {
        if (filter_var($path, FILTER_VALIDATE_URL) === FALSE) {
            $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
            $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
            $absolutes = array();
            foreach ($parts as $part) {
                if ('.' == $part) continue;
                if ('..' == $part) {
                    array_pop($absolutes);
                } else {
                    $absolutes[] = $part;
                }
            }
            return '/' . implode(DIRECTORY_SEPARATOR, $absolutes);
        }
        return $path;
    }
}


if (!function_exists('sc_define_content')) {
    function sc_define_content($key, $dev = [])
    {
        $result =  \KDA\Backpack\StructuredEditor\Models\StaticContent::current($key);
        if (env('APP_DEBUG') === true && !$result) {
            $contentKey = \KDA\Backpack\StructuredEditor\Models\StaticContentKey::where('name', $key)->first();
            if (!$contentKey) {
                $contentKey = new \KDA\Backpack\StructuredEditor\Models\StaticContentKey();
                $contentKey->name = $key;
                $contentKey->hint = $dev['key_hint'] ?? NULL;
                $contentKey->content_type_id = NULL;
                $type = NULL;
                if (isset($dev['type_key'])) {
                    $type = \KDA\Backpack\StructuredEditor\Models\StaticContentType::where('name', $dev['type_key'])->first();

                    if (!$type) {
                        $type = new \KDA\Backpack\StructuredEditor\Models\StaticContentType();
                        $type->name = $dev['type_key'];
                        $type->save();
                    }
                    $contentKey->contentType()->associate($type);
                }

                $contentKey->save();
            }
            $content = \KDA\Backpack\StructuredEditor\Models\StaticContent::where('content_key_id', $contentKey->id)->first();
            if (!$content) {
                \KDA\Backpack\StructuredEditor\Models\StaticContent::create(
                    [
                        'content_key_id' => $contentKey->id,
                        'value' => $default,
                        'enabled' => 1,
                    ]
                );
            }
        }
    }
}


if (!function_exists('sc_get_content')) {
    function sc_get_content($key, $default = NULL, $dev = [])
    {
        $result =  \KDA\Backpack\StructuredEditor\Models\StaticContent::current($key);
        if (env('APP_DEBUG') === true && !$result && $default!=NULL) {
            $contentKey = \KDA\Backpack\StructuredEditor\Models\StaticContentKey::where('name', $key)->first();
            if (!$contentKey) {
                $contentKey = new \KDA\Backpack\StructuredEditor\Models\StaticContentKey();
                $contentKey->name = $key;
                $contentKey->hint = $dev['key_hint'] ?? NULL;
                $contentKey->content_type_id = NULL;
                $type = NULL;
                if (isset($dev['type_key'])) {
                    $type = \KDA\Backpack\StructuredEditor\Models\StaticContentType::where('name', $dev['type_key'])->first();

                    if (!$type) {
                        $type = new \KDA\Backpack\StructuredEditor\Models\StaticContentType();
                        $type->name = $dev['type_key'];
                        $type->save();
                    }
                    $contentKey->contentType()->associate($type);
                }

                $contentKey->save();
            }
            $content = \KDA\Backpack\StructuredEditor\Models\StaticContent::where('content_key_id', $contentKey->id)->first();
            if (!$content) {
                \KDA\Backpack\StructuredEditor\Models\StaticContent::create(
                    [
                        'content_key_id' => $contentKey->id,
                        'value' => $default,
                        'enabled' => 1,
                    ]
                );
            }
        }
        return $result->value ?? $default;
    }
}

if (!function_exists('sc_bind')) {
    function sc_bind($content)
    {

        $data = [];
        foreach ($content->value as $field => $value) {
            $data[$field] = $value;
        }

        return $data;
    }
}


if (!function_exists('sc_get_view')) {
    function sc_get_view($view, $key, $more_args = [])
    {
        $result =  \KDA\Backpack\StructuredEditor\Models\StaticContent::current($key);
        if (!$result) {
            return '[CONTENT not found: ' . $key . ']';
        }

        $data = [];
        foreach ($result->value as $field => $value) {
            $data[$field] = $value;
        }

        return view($view, array_merge(['content' => $result], $data, $more_args));
    }
}

if (!function_exists('sc_render_bloc_bykey')) {
    function sc_render_bloc_bykey($key){
        $bloc = \KDA\SBC\Models\Bloc::where('name',$key)->first();
        return StructuredContent::renderBloc($bloc);
    }
}

