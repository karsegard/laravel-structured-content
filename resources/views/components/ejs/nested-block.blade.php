@props([
    'renderArticle' => true,
    'blocks'=>[]
])

@php
    $blocks =$blocks['blocks'] ??[];
@endphp

@if ($renderArticle)
    <article class="user-content">
@endif

    {{$slot}}


    @foreach($blocks as $block)

        @php
            $component = 'content-block.'.$block['type'].'-block';
        @endphp

        <x-dynamic-component :component="$component" :block="$block"/>

    @endforeach

@if ($renderArticle)
    </article>
@endif