@props(['type'=>'','field'=>NULL])
@php
    $component = 'sc::field-'.$type;
@endphp
<x-dynamic-component :component="$component" :content="$field"/>