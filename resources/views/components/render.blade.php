@props(['block'=>NULL])
@php
    $repeat= $block->block->type->repeatable;
    $is_repeating = $repeat['repeatable']===1;
    $types = $block->block->type->fields_by_type;
@endphp

@if($is_repeating)
    @foreach($block->value as $item)
        <x-sc::render-block :block="$item" :types="$types"/>
    @endforeach
@else

    <x-sc::render-block :block="$block->value" :types="$types"/>
@endif