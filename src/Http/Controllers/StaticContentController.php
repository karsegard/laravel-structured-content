<?php

namespace  KDA\SBC\Http\Controllers;

use Illuminate\Routing\Controller;
use KDA\SBC\Models\StaticPage;
use Illuminate\Support\Facades\View;
use KDA\SBC\Http\Controllers\Traits\RenderBlock;

use KDA\Sluggable\Facades\Slug;

class StaticContentController extends Controller
{
    use RenderBlock;
   
    public function byId($id){
        $page = Page::find($id);

        return  $this->renderPage($page);
    }

    
  
}
