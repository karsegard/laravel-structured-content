<?php

namespace  KDA\SBC\Http\Controllers\Traits;

use KDA\SBC\Facades\StructuredContent;
use KDA\SBC\Models\StaticContent;
use KDA\SBC\Models\StaticPage;
use KDA\SBC\Models\StaticContentType;

trait GenerateFields
{

    public function getTypes()
    {
        return StructuredContent::getTypes();
    }

    public function generateField($type, $tab = 'Contenu')
    {
        $ns = $type_types[$type['type']]['view_namespace'] ?? '';
        $namespace = [];
        if ($ns) {
            $namespace['view_namespace'] = $ns;
        }
        $options = $type_types[$type['type']]['options'] ?? [];
        return  array_merge(
            [
                'name' => $type['name'],
                'label' => $type['label'] ?? $type['name'],
                'type' => $type['type'],
                'wrapper' => (!empty($type['wrapper'])) ? $type['wrapper'] : [],
                'tab' => (!empty($type['tab'])) ? $type['tab'] : $tab,
                'hint' => $type['hint'],

            ],
            $namespace,
            $options
        );
    }


    public function generateFields($contentType, $name = NULL, $label = NULL, $tab = 'Contenu',$values=[])
    {
        $repeatable = $contentType->repeatable;
        $field_types = $this->getTypes();


        $is_repeatable = $repeatable['repeatable'] != "0";
        $fields = [];
        $definition = collect($contentType->fields_with_types);
        foreach ($definition->where('__type.key', '') as $field) {
            $ns = $field_types[$field['type']]['view_namespace'] ?? '';
            $namespace = [];
            $value = [];
            $settedValue = $values[$field['name']]??NULL;
            if($settedValue){
                $value['value']= $values[$field['name']];

            }
            if ($ns) {
                $namespace['view_namespace'] = $ns;
            }
            $options = $field_types[$field['type']]['options'] ?? [];
            $__options = $field['options'] ?? "";

            $__sbc_options = isset($field['sbc']) ? collect(json_decode($field['sbc'],true))->mapWithKeys(function($item){
                return [$item['key']=> json_decode($item['value'],true)];
            })->toArray(): [];
           
            $__options_values = collect(json_decode($__options, true))->mapWithKeys(function ($item) {
                return [$item['key'] => $item['value']];
            });
            $wrapper = $field['wrapper'] ?? [];
            $wrapper = is_string($wrapper) ? json_decode($wrapper,true): $wrapper;
            $wrapper = collect($wrapper)->reduce(function($carry,$item){
                $carry[$item['key']]=$item['value'];
                return $carry;
            },[]);
            $fields[] = array_merge(
                [
                    'name' => $name ? $name . '_' . $field['name'] : $field['name'],
                    'label' => $label ? '(' . $label . ') ' . $field['label'] : $field['label'],
                    'type' => $field['type'],
                    'options' => $__options_values,
                    'wrapper' => $wrapper,
                    'tab' => (!empty($field['tab'])) ? $field['tab'] : $tab,
                    'hint' => $field['hint'],
                    

                ],
                $value,
                $namespace,
                $options,
                $__sbc_options
              ///  $field['sbc']
            );
        }

        foreach ($definition->where('__type.key', '!=', '') as $field) {

            $fieldKey = StaticContentType::where('name', $field['__type']['key'])->first();
            $name = $field['name'];
            $label = $field['label'];
            $fields = array_merge($fields, $this->generateFields($fieldKey, $name, $label));
        }
        //  dump($fields);


        return $fields;
    }

    public function getSlotFieldName($slot){
        return $slot->id."_".$slot->pivot->slot->id;
    }

    public function generateSlots($contentKey)
    {
        $slots = [];
        $_slots = $contentKey->slots;
        foreach ($_slots as $slot) {

            $slot_name = $this->getSlotFieldName($slot);

            //  dump($slot);
           // $slotContent = StaticContent::where('content_key_id', $slot->id)->current()->first();
            $slotContent = StructuredContent::getCurrentContentForBlocId($slot->id);

            if ($slot->type->is_repeatable) {

                $slots[$slot_name] = $this->generateFields($slot->type, NULL, NULL, $slot->hint);

                $slots[$slot_name] = $this->createRepeatable(
                    $slots[$slot_name],
                    'slot_' . $slot_name,
                    $slotContent->value,
                    $slot->hint,
                    [],
                    $slot->hint
                );
            }else {
                $slots[$slot_name] = $this->generateFields($slot->type, $slot_name, NULL, $slot->hint,$slotContent->value);

            }
        }
        return $slots;
    }


    public function createRepeatable($fields, $name, $value = '', $label, $opts = [], $tab = '')
    {
        return [[
            'type' => "repeatable",
            'name' => $name,
            'label' => $label,
            'tab' => $tab,
            'fields' =>  $fields,
            'value' => $value

        ]];
    }
}
