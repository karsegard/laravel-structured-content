<?php

namespace  KDA\SBC\Http\Controllers\Traits;

use KDA\SBC\Models\Page;
use KDA\SBC\Models\StaticContent;
use Illuminate\Support\Facades\View;
use KDA\SBC\Facades\StructuredContent;

trait RenderBlock
{
    

    public function renderPage($page)
    {
        $views=[];
        foreach ($page->sections as $section) {

            $views[] = StructuredContent::renderSection($section);
        }

        return view(StructuredContent::getTemplate($page), ['views' => $views,'page'=>$page]);
    }


    public function renderContent($content)
    {

        return StructuredContent::renderContent($content);
    }

   

}
