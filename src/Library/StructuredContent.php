<?php

namespace  KDA\SBC\Library;

use KDA\SBC\Models\Content;

use KDA\SBC\Models\Type;
use Illuminate\Support\Facades\View;

class StructuredContent
{

    public function getTypes()
    {
        return array_merge(
            $this->getRawTypes(),
            $this->getRawTypesFromDb()
        );
    }
    public function getRawTypes()
    {
        return sc_config('field_types');
    }

    public function getRawTypesFromDb()
    {
        $types = Type::where('is_type', true)->get();
        $types =  $types->mapWithKeys(function ($q) {
            return [$q['name'] => [
                'label' => $q['description'],
                'key' => $q['name']
            ]];
        })->toArray();
        return $types;
    }

    public function getCurrentContentForBlocId($id, $auto_create = true)
    {
        $content = Content::where('bloc_id', $id)->current()->first();
        if ($auto_create && !$content) {
            $content = new Content();
            $content->bloc_id = $id;
            $content->save();
        }
        return $content;
    }

    public function applyViewComposers($entity, $template)
    {
        if ($entity->view_composers) {
            foreach ($entity->view_composers as $composer) {
                if (!class_exists($composer->class)) {
                    return 'Composer class does not exist ' . $composer->class;
                }
                View::composer($template, $composer->class);
            }
        }
    }

    public function renderContent($content)
    {
        $data = $content->value ?? [];
        if ($content->bloc->type->is_repeatable) {
            return  $this->renderRepeatableContent($content, $data);
        }
        return $this->renderSingleContent($content, $data);
    }

    public function renderSingleContent($content, $data)
    {
        $types = $content->bloc->type->all_fields_with_types;

        foreach ($data as $key => $d) {
            $type = $types->get($key);
            if ($type) {
                $data[$key] = view('sc::render-field', [
                    'field' => $d,
                    'type' => $type
                ]);
            }
        }
        return $data;
    }

    public function renderRepeatableContent($content, $data = [])
    {
        $types = $content->bloc->type->all_fields_with_types;
        foreach ($data as $id => $item) {

            foreach ($item as $key => $d) {
                $type = $types->get($key);
                if ($type) {
                    $data[$id][$key] = view('sc::render-field', [
                        'field' => $d,
                        'type' => $type
                    ]);
                }
            }
        }
        return $data;
    }

    public function getTemplate($entity)
    {
        $prefix = sc_config('template_prefix', '');
        if ($prefix) {
            $prefix .= ".";
        }
        $template = $entity->template;
        if (!$template || empty($template)) {
            return NULL;
        }
        return $prefix . $template;
    }

    public function renderSlot(\KDA\SBC\Models\Bloc $slot){
        $slotContent = $this->getCurrentContentForBlocId($slot->id);
        $render = $this->renderContent($slotContent);
        if ($slot->type->is_repeatable) {
            $render = ['value' => $render];
        }
        $tpl = $this->getTemplate($slot);

        if (!$tpl) {
            return 'No template defined for entity ' . $slot->id . ' of type ' . get_class($slot);
        } else {
            $this->applyViewComposers($slotContent,$tpl);
            return view($tpl, $render);
        }
    }

    public function renderBloc(\KDA\SBC\Models\Bloc $bloc,$template=NULL){
        $content = $this->getCurrentContentForBlocId($bloc->id);

        $template = $template ?? $this->getTemplate($bloc);
    
        $slots = $content->bloc->slots;
        $slots_data = [];
        $this->applyViewComposers($bloc,$template);
        foreach ($slots as $slot) {
            $slot_name = $slot->pivot->slot->name;
            $slots_data[$slot_name][] = $this->renderSlot($slot);
        }
        
        $data = $this->renderContent($content);
        if ($bloc->type->is_repeatable) {
            $data = ['value' => $data];
        }

        $vars = array_merge( $data, $slots_data);


        $vars['content'] = $content;

        if ($template) {
            return  view($template, $vars);
        } else {
            return 'Template not defined for section block ' . $bloc->name . ' of type ' . $bloc->type->name;
        }
    }

    /*
    Render a section
    */
    public function renderSection($section)
    {
        $content = $this->getCurrentContentForBlocId($section->bloc_id);

        $template = $this->getTemplate($section);

        $layout = $section->layoutData;

        $data = $content->value ?? [];

        $slots = $content->bloc->slots;
        $slots_data = [];
        foreach ($slots as $slot) {
            $slot_name = $slot->pivot->slot->name;
            $slotContent = $this->getCurrentContentForBlocId($slot->id);
            $render = $this->renderContent($slotContent);
            if ($slot->type->is_repeatable) {
                $render = ['value' => $render];
            }
            $tpl = $this->getTemplate($slot);

            if (!$tpl) {
                $slots_data[$slot_name] = 'No template defined for entity ' . $slot->id . ' of type ' . get_class($slot);
            } else {
                $this->applyViewComposers($slotContent,$tpl);
                $slots_data[$slot_name][] = view($tpl, $render);
            }
        }
    
        $data = $this->renderContent($content);
        if ($section->bloc->type->is_repeatable) {
            $data = ['value' => $data];
        }

        $vars = array_merge($layout, $data, $slots_data);

        $vars['content'] = $content;
        if ($template) {
         
            return  view($template, $vars);
        } else {
            return 'Template not defined for section block ' . $content->bloc->name . ' of type ' . $content->bloc->type->name;
        }
    }
}
