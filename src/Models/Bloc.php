<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Str;
class Bloc extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type_id',
        'hint',
        'layout_type_id',
        'template',
        'is_craftable',
        'is_linkable'
    ];

    protected $table= "sbc_blocs";
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_id' => 'integer',
        'is_craftable'=>'boolean',
        'is_linkable'=>'boolean'
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
           $model->setDefaultValues();
        });

        static::created(function($model){
            $model->createSubContent();
        });
        static::updating(function ($model)
        {
           $model->setDefaultValues();
        });
    }

    public function setDefaultValues(){
        if(empty($this->attributes['hint'])){
            $this->attributes['hint']= Str::slug($this->attributes['name']);
        }
        if(empty($this->attributes['name'])){
            $this->attributes['name']= Str::slug($this->attributes['hint']);
        }
    }

    public function createSubContent(){
       $slots = $this->type->slots;
       if($slots ){
            foreach($slots as $slot){

                $creatable = $slot->creatable->first();
               
                if($creatable){
                    $b = Bloc::create([
                        'type_id'=>$creatable->type_id,
                        'hint'=>$this->hint.' '.$slot->name,
                        'name'=>Str::slug($this->hint.' '.$slot->name.' '.$creatable->description),
                    ]);
                    $this->slots()->attach($b,['slot_id'=>$slot->id]);
//                dd($creatable,$b);

                }
            }

       }

       $has_content = Content::current($this->name);
       if (!$has_content) {
           Content::create([
               'bloc_id' => $this->id
           ]);
       }



    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }


    public function layout()
    {
        return $this->belongsTo(Type::class,'layout_type_id');
    }

    public function getCollectionAttribute(){
        return $this->type ? $this->type->collection: NULL;
}

    public function composers(){
        return $this->belongsToMany(
            ViewComposer::class,
            'sbc_bloc_view_composer',
            'bloc_id',
            'composer_id'
            
        );
    }

    public function slots(){
        return  $this->belongsToMany(
            Bloc::class,
            'sbc_bloc_slot',
            'bloc_id',
            'slot_bloc_id'
            
        )->using(Relations\BlocSlot::class)->withPivot('slot_id')->orderBy('sort');
    }

    public function children(){
        return  $this->belongsToMany(
            Bloc::class,
            'sbc_bloc_slot',
            'bloc_id',
            'slot_bloc_id'
            
        )->using(Relations\BlocSlot::class)->as('parentSlot')->withPivot('slot_id')->orderBy('sort');
    }
    
  /*  public function slots(){
        return $this->belongsToMany(
            Bloc::class,
            'sbc_bloc_slot',
            'bloc_id',
            'slot_bloc_id'
            
        )->withPivot('slot_id');
    }
*/
    public function slotteds(){
        return $this->belongsToMany(
            Bloc::class,
            'sbc_bloc_slot',
            'slot_bloc_id',
            'bloc_id'
            
        )->using(Relations\BlocSlot::class)->withPivot('slot_id');
    }

    public function scopeWithNoParentBlock($q){
        return $q->whereDoesntHave('slotteds');
    }
    public function scopeWithParentBlock($q){
        return $q->whereHas('slotteds');
    }

    public function contents(){
        return $this->hasMany(
            Content::class,
            'bloc_id'
            
        );
    }
    public function content(){
        return $this->hasOne(
            Content::class,
            'bloc_id'
        )->latestOfMany();
    }
    public function getSlotsListAttribute() {
        return json_encode($this->slots->map(function($slot) {
            return ['slot_bloc_id' => $slot->id, 'slot_id' => $slot->pivot->slot->id];
        }));
    }

    public function getTemplateAttribute()
    {
        $template =  $this->attributes['template'] ?? '' ;
        if(!$template && $this->type) {
            $template = $this->type->template;
        }
        return $template;
    }

    public function getLayoutDataAttribute()
    {
    //   return $this->attributes['layout'] ?? $this->type->default_layout;
        $layout = $this->layout;
        
        if($layout){
            return $layout->default_layout;
        }
        return NULL;
    }
    public function getViewComposersAttribute()
    {
        $composers = $this->composers ?? NULL;
        return $composers;
    }
    public function getFullAttribute(){
        return $this->hint.' (type: '.$this->name.')';
    }

    protected static function newFactory()
    {
        return  \KDA\SBC\Database\Factories\BlocFactory::new();
    }



    public function pages(){
        return $this->hasManyThrough(StaticPage::class,Content::class,'bloc_id','id','id','page_id');
    }

}
