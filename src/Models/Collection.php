<?php

namespace KDA\SBC\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Collection extends Model
{
    use HasFactory;

    protected $table= 'sbc_collections';

    protected $fillable = [
        'id',
        'name'
    ];

    protected $casts = [
        'id' => 'integer',

    ];

    

    protected static function newFactory()
    {
        return  \KDA\SBC\Database\Factories\CollectionFactory::new();
    }

    
}
