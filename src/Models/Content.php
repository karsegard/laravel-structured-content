<?php

namespace KDA\SBC\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Content extends Model
{
        use HasFactory;
        use \KDA\Laravel\Models\Traits\Schedulable;

        protected $table = "sbc_contents";
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                'bloc_id',
                'value',
                'start_on',
                'end_on',
                'enabled',
        ];

        protected $casts = [
                'id' => 'integer',
                'bloc_id' => 'integer',
                'start_on' => 'date',
                'end_on' => 'date',
                'enabled' => 'boolean',
                'value' => 'array',
        ];
        protected $fakeColumns = [
                'value',

        ];

        protected $schedulable = [
                'start' => 'start_on',
                'end' => 'end_on',
                'enabled' => 'enabled'
        ];
        public function bloc()
        {
                return $this->belongsTo(Bloc::class);
        }

        public function schedulableGroup($q, $key)
        {
                return $q->whereHas('bloc', function ($q) use ($key) {
                        $q->where('name', $key);
                });
        }

        public function getSchedulableGroupAttribute()
        {
                return $this->bloc->name;
        }

        public function getTypesAttribute()
        {
                return $this->bloc->contentType->fields_by_type;
        }

        public function getTemplateAttribute()
        {
              /*  $prefix = sc_config('template_prefix', '');
                if ($prefix) {
                        $prefix .= ".";
                }*/
                $template =  $this->attributes['template'] ?? $this->bloc->template;

                return $template;
        }

        public function getLayoutDataAttribute()
        {
                return $this->attributes['layout'] ?? $this->bloc->layoutData;
        }
        public function getViewComposersAttribute()
        {

                $composers = $this->attributes['composers'] ?? $this->bloc->view_composers;
                return $composers;
        }

        protected static function newFactory()
        {
                return  \KDA\SBC\Database\Factories\ContentFactory::new();
        }

       

        /*
        public function pages()
        {
                return $this->hasManyThrough(StaticPage::class, StaticPageContent::class, 'bloc_id', 'id', 'bloc_id', 'page_id');
                //3 -> local id

        }*/
}
