<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $table = "sbc_pages";


    protected $fillable = [
        'name',
        'template',
      
    ];


    public function sections()
    {
        return $this->hasMany(Section::class, 'page_id')->orderBy('lft');
    }

    public function identifiableAttribute(){
        return $this->name;
    }
}
