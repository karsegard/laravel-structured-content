<?php

namespace KDA\SBC\Models;


use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
        protected $table = "sbc_page_contents";
      
        protected $fillable =[
                'bloc_id',
                'page_id',
                'layout'
        ];
        protected $casts=[
                'layout'=>'array'
        ];
        protected $fakeColumns = [
                'layout',

        ];


        public function scopeForPage($query, $id)
        {
                return $query->where('page_id', $id);
        }


        public function bloc()
        {
                return $this->belongsTo(Bloc::class);
        }


       

        public function content()
        {
                return $this->hasOne(
                        Content::class, 
                        'bloc_id',
                        'bloc_id'
                )->ofMany([],function($q){
                        return $q->current();
                });
        }

        public function page()
        {
                return $this->belongsTo(Page::class,'page_id');
        }

        public function getTemplateAttribute(){
                $template =  $this->attributes['template'] ?? $this->content->template;
                return $template;
        }
        public function getLayoutDataAttribute(){
                return $this->layout ?? $this->content->layout;
        }

        public function getViewComposersAttribute(){
                $composers = $this->attributes['composers'] ?? $this->content->view_composers;
                return $composers;
        }
        
}
