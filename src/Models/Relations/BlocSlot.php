<?php
namespace KDA\SBC\Models\Relations;

use Illuminate\Database\Eloquent\Relations\Pivot;
use KDA\SBC\Models\Bloc;
use KDA\SBC\Models\Slot;

class BlocSlot extends Pivot
{
    protected $table = 'sbc_bloc_slot';

    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function parent()
    {
        return $this->belongsTo(Bloc::class);
    }

    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }

    public function bloc(){
        return $this->belongsTo(Bloc::class,'slot_bloc_id');
    }
}