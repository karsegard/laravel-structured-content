<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Slot extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'sbc_slots';
    protected $fillable = [
        'bloc_id',
        'name',
        'type_id'
    ];


    public function bloc(){
        return $this->belongsTo(Bloc::class);
    }


    public function type(){
        return $this->belongsTo(Type::class);
    }


    public function blocPolicy(){
        return $this->hasMany(SlotPolicy::class);
    }

    public function creatable(){
        return $this->blocPolicy()->where('default',1)->where('rule','accept');
    }

    protected static function newFactory()
    {
        return  \KDA\SBC\Database\Factories\SlotFactory::new();
    }
    
    public function identifiableAttribute(){
        return $this->name;
    }
}
