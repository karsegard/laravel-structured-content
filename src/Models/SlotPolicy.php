<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class SlotPolicy extends Model
{
    public $timestamps = false;
    protected $table = 'sbc_slot_types';
    protected $fillable = [
        'slot_id',
        'type_id',
        'rule',
        'default',
    ];


    public function slot(){
        return $this->belongsTo(Slot::class);
    }


    public function type(){
        return $this->belongsTo(Type::class);
    }
    public function identifiableAttribute(){
        return $this->slot ? $this->slot->name : '';
    }
}
