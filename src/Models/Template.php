<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Template extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'sbc_templates';
    protected $fillable = [
        'description',
        'path',
        'test_data'
    ];

    protected static function newFactory()
    {
        return  \KDA\SBC\Database\Factories\TemplateFactory::new();
    }
}
