<?php


namespace KDA\SBC\Models\Traits;



trait HasSections
{

    public function sections()
    {
        return $this->hasMany(Section::class, 'page_id')->orderBy('lft');
    }
}
