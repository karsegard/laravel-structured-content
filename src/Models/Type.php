<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Models\Traits\HasDefaultAttributes;
use Str;

class Type extends Model
{
        use HasFactory;
        use HasDefaultAttributes;
        protected $table = 'sbc_types';
        protected $fillable = [
                'name',
                'description',
                'fields',
                'repeatable',
                'is_block',
                'is_type',
                'is_layout',
                'template',
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
                'id' => 'integer',
                'is_block' => 'boolean',
                'is_type' => 'boolean',
                'repeatable' => 'array',
                'fields' => 'array',
                'is_layout' => 'boolean',
        ];


        protected $fakeColumns = [
                'repeatable'
        ];

        public function setDefaultValues()
        {
                if (empty($this->attributes['name'])) {
                        $this->attributes['name'] = Str::slug($this->attributes['description']);
                }
        }
        public function slots()
        {
                return $this->hasMany(Slot::class, 'type_id', 'id');
        }

        public function collection()
        {
                return $this->belongsTo(Collection::class);
        }

        //accessor to get the comments populated in the repeatable field again
        public function getSlotsListAttribute()
        {
                return json_encode($this->slots);
        }

        public function getFieldsByTypeAttribute()
        {
                return collect($this->fields)->pluck('type', 'name');
        }

        public function getDefaultLayoutAttribute()
        {
                $layout = $this->attributes['layout'] ?? "[]";
                $value = json_decode($layout, true);
                $value = collect($value)->mapWithKeys(function ($i) {
                        return [$i['name'] => $i['default'] ?? ''];
                })->toArray();
                return $value;
        }

        public function getIsRepeatableAttribute()
        {
                $repeatable = $this->repeatable['repeatable'] ?? 0;
                return $repeatable != 0;
        }

        public function getFieldsWithTypesAttribute()
        {
                $types = app()->make('scm-library')->getTypes();
                return collect($this->fields)->map(function ($i) use ($types) {
                        $i['__type'] = $types[$i['type']];
                        return $i;
                });
        }

        public function getAllFieldsWithTypesAttribute()
        {
                $types = app()->make('scm-library')->getTypes();
                return collect(collect($this->fields)->reduce(function ($carry, $i, $type_key) use ($types) {
                        $type = $i['type'];

                        $t = $types[$i['type']];
                        $key = $t['key'] ?? NULL;
                        //   dump($i);
                        if ($key) {
                                $t = self::where('name', $key)->first();
                                $sub = $t->all_fields_with_types;
                                foreach ($sub as $field_name => $subtype) {
                                        $carry[$i['name'] . '_' . $field_name] = $subtype;
                                }
                        } else {
                                $carry[$i['name']] = $type;
                        }
                        return $carry;
                }, []));
        }


        protected static function newFactory()
        {
                return  \KDA\SBC\Database\Factories\TypeFactory::new();
        }
        public function identifiableAttribute(){
                return $this->description;
            }
}
