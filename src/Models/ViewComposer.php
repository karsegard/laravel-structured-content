<?php

namespace KDA\SBC\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ViewComposer extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'class',
        'description'
    ];

    protected $table = "sbc_view_composers";
    
    public function blocs(){
        return 
        $this->belongsToMany(
            Bloc::class,
            'sbc_bloc_view_composer',
            'composer_id',
            'bloc_id',
            'id',
            'id'
        );
    }

    protected static function newFactory()
    {
        return  \KDA\SBC\Database\Factories\ViewComposerFactory::new();
    }
}

