<?php

namespace KDA\SBC;


use KDA\Laravel\PackageServiceProvider;
use Illuminate\Support\Facades\Blade;

class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasHelper;
    //use \KDA\Laravel\Traits\HasComponents;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasComponentNamespaces;

    use \KDA\Laravel\Traits\HasDumps;
    
    protected $viewNamespace = 'sc';
    protected $publishViewsTo = 'vendor/kda/sc';

  /*  protected $components = [
        'sc-render'=> View\Components\Render::class,
        'sc-render-block'=> View\Components\RenderBlock::class,
        'sc-render-field'=> View\Components\RenderField::class,
        'sc-field-editorjs'=> View\Components\FieldEditorjs::class,
        'sc-field-text'=> View\Components\FieldText::class,
        'sc-field-browse'=> View\Components\FieldBrowse::class,
        'sc-field-select2_from_array'=> View\Components\FieldSelect::class,
        'sc-ejs-block'=> View\Components\EditorJS\Block::class,
        'sc-ejs-image'=> View\Components\EditorJS\Image::class,
        'sc-ejs-header'=> View\Components\EditorJS\Header::class,
        'sc-ejs-nested'=> View\Components\EditorJS\Nested::class,
        'sc-ejs-list'=> View\Components\EditorJS\Listing::class,
        'sc-ejs-paragraph'=> View\Components\EditorJS\Paragraph::class,
        'sc-ejs-table'=> View\Components\EditorJS\Table::class,
        'sc-ejs-render'=> View\Components\EditorJS\Render::class,
    ];

*/
    protected $anonymousComponentNamespaces = [
        'components' => 'sc'
    ];

    protected $configs= [
        'kda/structured_editor.php'  =>'kda.sbc'
    ];

    protected $dumps = [
        'sbc_blocs',
        'sbc_collections',
        'sbc_contents',
        'sbc_pages',
        'sbc_sections',
        'sbc_slots',
        'sbc_templates',
        'sbc_types',
        'sbc_view_composers',
        'sbc_bloc_view_composer',
        'sbc_bloc_slot',
        'sbc_slot_types',
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register(){
        parent::register();
        $this->app->singleton('scm-library',function($q){
            return new Library\StructuredContent();
        });
    }

    protected function bootSelf(){
        Blade::directive('bind_values', function ($content) {
            return "<?php 
                foreach (\$content  as \$field => \$value) {
                    \$\$field = \$value;
                }
    
            ?>";
        });
    }


   

}
