<?php

namespace KDA\Tests\Composers;

use Illuminate\View\View;
use KDA\Tests\Models\Post;


use Illuminate\Http\Request ;
class PostComposer
{

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {

      
        $view->with(
            ['posts'=> Post::all()],
        );
    }
}
