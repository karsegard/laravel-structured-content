<?php

namespace KDA\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Bloc;
use KDA\SBC\Models\Type;
use KDA\SBC\Models\Collection;
use KDA\SBC\Models\Content;
use KDA\SBC\Models\Section;
use KDA\Tests\Models\Post;
use KDA\SBC\Facades\StructuredContent;
use KDA\SBC\Models\ViewComposer;

use KDA\Tests\TestCase;

class RenderFeatureTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_block_render_single_content()
  {


    $fields = [
      [
        "label" => "Nom",
        "name" => "name",
        "type" => "text",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ],
      [
        "label" => "Image",
        "name" => "image",
        "type" => "browse",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ]
    ];

    $repeatable = [
      "repeatable" => "0",
      "init_rows" => "0",
      "min_rows" => "0",
      "max_rows" => "0"
    ];


    $t = Type::factory()->create(['name' => 'team', 'fields' => $fields, 'repeatable' => $repeatable]);


    $b = Bloc::factory()->create(['name' => 'test', 'type_id' => $t->id, 'template' => 'sc::tests.team']);


    $values =
      [
        'name' => 'Fabien',
        'image' => '/blabla.jpg'
      ];


    $c = Content::create(['bloc_id' => $b->id, 'value' => $values]);

    $r = StructuredContent::renderContent($c);

    $this->assertEquals(2, count($r));
  }


  /** @test */
  function a_block_render_repeatable_content()
  {


    $fields = [
      [
        "label" => "Nom",
        "name" => "name",
        "type" => "text",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ],
      [
        "label" => "Image",
        "name" => "image",
        "type" => "browse",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ]
    ];

    $repeatable = [
      "repeatable" => "1",
      "init_rows" => "0",
      "min_rows" => "0",
      "max_rows" => "0"
    ];


    $t = Type::factory()->create(['name' => 'team', 'fields' => $fields, 'repeatable' => $repeatable]);


    $b = Bloc::factory()->create(['name' => 'test', 'type_id' => $t->id, 'template' => 'sc::tests.team']);


    $values = [
      [
        'name' => 'Fabien',
        'image' => '/blabla.jpg'
      ],
      [
        'name' => 'Fabien',
        'image' => '/blabla.jpg'
      ]
    ];


    $c = Content::create(['bloc_id' => $b->id, 'value' => $values]);

    $r = StructuredContent::renderContent($c);

    $this->assertEquals(2, count($r));
    $this->assertEquals(2, count($r[0]));
  }



  /** @test */
  function a_section_can_render_repeatable()
  {


    $fields = [
      [
        "label" => "Nom",
        "name" => "name",
        "type" => "text",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ],
      [
        "label" => "Image",
        "name" => "image",
        "type" => "browse",
        "hint" => null,
        "tab" => null,
        "wrapper" => null,
        "options" => null
      ]
    ];

    $repeatable = [
      "repeatable" => "1",
      "init_rows" => "0",
      "min_rows" => "0",
      "max_rows" => "0"
    ];


    $t = Type::factory()->create(['name' => 'team', 'fields' => $fields, 'repeatable' => $repeatable]);


    $b = Bloc::factory()->create(['name' => 'test', 'type_id' => $t->id, 'template' => 'sc::tests.team']);


    $values = [
      [
        'name' => 'Fabien',
        'image' => '/blabla.jpg'
      ],
      [
        'name' => 'Fabien',
        'image' => '/blabla.jpg'
      ]
    ];

    $c = Content::create(['bloc_id' => $b->id, 'value' => $values]);

    $s = Section::create(['bloc_id' => $b->id]);

    $r = StructuredContent::renderSection($s);

    $result = $r->render();
    $expectedResult = "/blabla.jpg\nFabien\n/blabla.jpg\nFabien\n";
    $this->assertEquals($expectedResult, $result);
  }


  /** @test */
  function a_section_can_render_composer()
  {


    $repeatable = [
      "repeatable" => "0",
      "init_rows" => "0",
      "min_rows" => "0",
      "max_rows" => "0"
    ];


    $t = Type::factory()->create(['name' => 'team', 'fields' => NULL, 'repeatable' => $repeatable]);

    $vc = ViewComposer::factory()->create(['description' => 'Fake Title','class'=>'\KDA\Tests\Composers\PostComposer']);

    $b = Bloc::factory()->create(['name' => 'test', 'type_id' => $t->id, 'template' => 'sc::tests.composers']);
    $b->composers()->sync($vc);

    $c = Content::create(['bloc_id' => $b->id, 'value' => NULL]);

    $s = Section::create(['bloc_id' => $b->id]);

    $r = StructuredContent::renderSection($s);

    Post::factory()->create(['title'=>'coucou']);
    Post::factory()->create(['title'=>'coucou']);

    //$result = $r->render();

    $result = $r->render();
    $this->assertEquals("2",$result);
  }



  /** @test */
  function invalid_composer_should_return_error_string()
  {


    $repeatable = [
      "repeatable" => "0",
      "init_rows" => "0",
      "min_rows" => "0",
      "max_rows" => "0"
    ];


    $t = Type::factory()->create(['name' => 'team', 'fields' => NULL, 'repeatable' => $repeatable]);

    $vc = ViewComposer::factory()->create(['description' => 'Fake Title','class'=>'\poser']);

    $b = Bloc::factory()->create(['name' => 'test', 'type_id' => $t->id, 'template' => 'sc::tests.composers']);
    $b->composers()->sync($vc);

    $c = Content::create(['bloc_id' => $b->id, 'value' => NULL]);

    $s = Section::create(['bloc_id' => $b->id]);

    $r = StructuredContent::renderSection($s);

    Post::factory()->create(['title'=>'coucou']);

    $this->assertIsString($r);
    $this->assertEquals($r,"Composer class does not exist \poser");

  }
}
