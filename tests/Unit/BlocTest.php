<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Bloc;
use KDA\SBC\Models\Type;
use KDA\SBC\Models\ViewComposer;
use KDA\SBC\Models\Collection;

use KDA\Tests\TestCase;

class BlocTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_bloc_has_a_name()
  {
    $o = Bloc::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->name);
  }


 /** @test */
 function a_bloc_has_a_template()
 {
   $o = Bloc::factory()->create(['name' => 'Fake Title','template'=>'test']);
   $this->assertEquals('test', $o->template);
 }


  /** @test */
  function a_bloc_has_a_type()
  {


    $t = Type::factory()->create(['name' => 'Fake Title']);

    $o = Bloc::factory()->create(['name' => 'Fake Title','type_id'=>$t]);
    $this->assertEquals($o->type->id, $t->id);
  }


  /** @test */
  function a_bloc_has_a_type_template()
  {


    $t = Type::factory()->create(['name' => 'Fake Title','template'=>'parent_template']);

    $o = Bloc::factory()->create(['name' => 'Fake Title','type_id'=>$t]);
    $this->assertEquals($o->template, "parent_template");
  }

  /** @test */
  function a_bloc_has_a_collection_through()
  {

    $c = Collection::factory()->create(['name'=>'Pages']);

    $t = Type::factory()->create(['name' => 'Fake Title','collection_id'=>$c->id]);

    $o = Bloc::factory()->create(['name' => 'Fake Title','type_id'=>$t]);
    $this->assertEquals($o->collection->id, $c->id);
  }

  /** @test */
  function a_bloc_has_composers()
  {

    $vc =  ViewComposer::factory()->create(['description' => 'Fake Title','class'=>'\KDA\Tests\Composers\PostComposer']);

    $t = Type::factory()->create(['name' => 'Fake Title','template'=>'parent_template']);

    $o = Bloc::factory()->create(['name' => 'Fake Title','type_id'=>$t]);

    $o->composers()->sync($vc);


    $this->assertEquals($o->template, "parent_template");
    $this->assertEquals($o->composers->count(), 1);
  }

}
