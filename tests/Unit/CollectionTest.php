<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Collection;

use KDA\Tests\TestCase;

class CollectionTest extends TestCase
{
  use RefreshDatabase;

  
  /** @test */
  function a_collection_has_a_name()
  {
    $o = Collection::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->name);

  }


  
}