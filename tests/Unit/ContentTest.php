<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Content;
use KDA\SBC\Models\Bloc;

use KDA\Tests\TestCase;

class ContentTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_content_has_a_name()
  {

    $b = Bloc::factory()->create(['name' => 'Fake Title']);
    $o = Content::factory()->create(['bloc_id' => $b->id]);
    $this->assertEquals($b->id, $o->bloc->id);
  }
}
