<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use KDA\SBC\Models\Section;
use KDA\SBC\Models\Bloc;
use KDA\SBC\Models\Type;

use KDA\Tests\TestCase;

class SectionTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_section_has_a_bloc()
  {
    $t = Type::factory()->create(['name' => 'Fake Title']);
    $b = Bloc::factory()->create(['name'=>'bloc','type_id'=>$t->id]);
    $o = Section::factory()->create(['bloc_id'=>$b->id]);
    $this->assertEquals($b->id, $o->bloc->id);
  }

}
