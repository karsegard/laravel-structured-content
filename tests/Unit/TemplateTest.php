<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Template;

use KDA\Tests\TestCase;

class TemplateTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_template_has_path()
  {
    $o = Template::factory()->create(['path' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->path);
  }

  /** @test */
  function a_template_has_path_fails()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);

    $o = Template::factory()->create(['description'=>'test']);
  }
}
