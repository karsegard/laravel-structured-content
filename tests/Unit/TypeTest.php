<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\Type;
use KDA\SBC\Models\Collection;

use KDA\Tests\TestCase;

class TypeTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_type_has_a_name()
  {
    $o = Type::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->name);
  }

    /** @test */
    function a_type_has_a_collection()
    {

      $c = Collection::factory()->create(['name'=>'Pages']);

      $o = Type::factory()->create(['name' => 'Fake Title','collection_id'=>$c->id]);
      $this->assertEquals('Fake Title', $o->name);
      $this->assertEquals($c->id, $o->collection->id);
    }
}
