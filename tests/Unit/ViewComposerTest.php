<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SBC\Models\ViewComposer;

use KDA\Tests\TestCase;

class ViewComposerTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_composer_has_description_and_class()
  {
    $o = ViewComposer::factory()->create(['description' => 'Fake Title','class'=>'hello world']);
    $this->assertEquals('Fake Title', $o->description);
    $this->assertEquals('hello world', $o->class);
  }


  /** @test */
  function a_composer_has_description_fails()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);

    $o = ViewComposer::factory()->create(['description' => 'Fake Title']);
  }

  /** @test */
  function a_composer_has_class_fails()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);

    $o = ViewComposer::factory()->create(['class' => 'Fake Title']);
  }


}
