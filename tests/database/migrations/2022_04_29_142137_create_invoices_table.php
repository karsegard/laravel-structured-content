<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

      /*  Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

        });

        Schema::create('invoice_lines', function (Blueprint $table) {
            $table->id();
            $table->string('group');
            $table->foreignId('invoice_id')->constrained('invoices')->onDelete('cascade');
            $table->timestamps();
        });
*/
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_lines');
    }
}
